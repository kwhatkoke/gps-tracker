package kz.aitu.team1.writer.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "loc")
public class Location {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Column(name="LATITUDE", length=50, nullable=false, unique=false)
    private Double lat;

    @Column(name="LONGITUDE", length=50, nullable=false, unique=false)
    private Double lon;

    public Location() { }
    public Location(Double lat, Double lon){
        this.lat = lat;
        this.lon = lon;
    }

    public Long getId() {
        return id;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) { return true; }
        if (obj == null) { return false; }
        final Location other = (Location) obj;
        return Objects.equals(this.lat, other.lat) && Objects.equals(this.lon, other.lon);
    }

    @Override
    public String toString() {
        return this.lat.toString() + " " + this.lon.toString();
    }
}
