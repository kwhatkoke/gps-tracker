package kz.aitu.team1.writer.controller;

import kz.aitu.team1.writer.model.Location;
import kz.aitu.team1.writer.repository.LocationRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping(value = "/location")
public class WriteLocationController {
    private final LocationRepository locRepository;
    public WriteLocationController(LocationRepository locRepository) { this.locRepository = locRepository; }

    @PostMapping("/add-tracking")
    public String addLocation(@RequestBody Location location) {
        locRepository.save(location);
        return location.getLat().toString() + " " + location.getLon().toString();
    }
}
