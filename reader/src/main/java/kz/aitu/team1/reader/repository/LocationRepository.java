package kz.aitu.team1.reader.repository;

import kz.aitu.team1.reader.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> { }