package kz.aitu.team1.reader.controller;

import kz.aitu.team1.reader.model.Location;
import kz.aitu.team1.reader.repository.LocationRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/location")
public class ReadLocationController {

    public final LocationRepository locRepository;
    public ReadLocationController(LocationRepository locRepository) {
        this.locRepository = locRepository;
    }

    @GetMapping(value = "/get", produces = "application/json")
    public Location getLocation() {
        List<Location> locations = locRepository.findAll();
        return locations.get(locations.size() - 1);
    }
}