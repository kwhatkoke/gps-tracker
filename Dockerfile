FROM openjdk:16

WORKDIR /
ARG CI_PROJECT_NAME
ADD target/${CI_PROJECT_NAME}.jar app.jar

CMD [ "sh", "-c", "java -Xms64m -Xmx64m -jar /app.jar $JAVA_APP_ARGS" ]
